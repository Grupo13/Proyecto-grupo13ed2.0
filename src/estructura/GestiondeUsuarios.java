
package estructura;

import Metodos.conexion;
import Metodos.procedimientos_BD_USUARIOS;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.table.DefaultTableModel;

public final class GestiondeUsuarios extends javax.swing.JFrame {

    //Para establecer el modelo al JTable
    procedimientos_BD_USUARIOS usuario = new procedimientos_BD_USUARIOS();
    DefaultTableModel modelo = new DefaultTableModel();
    
    static Connection conn = null;
    //PARA LA CONEXION
    static Statement s = null;
    //PARA ESTABLECER UNA CONSULTA
    static ResultSet rs = null;
    //PARA BRINDAR UN RESULTADO
    
    public GestiondeUsuarios() {
        initComponents();
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs = conexion.USUARIO(rs);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
            //Establecer como cabezeras el nombre de las colunnas
            for (int i = 1; i <= cantidadColumnas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        }
       usuario.mostrar(modelo);
        GestiondeUsuarios.TBADMI.setModel(modelo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        TBADMI = new javax.swing.JTable();
        TXTNombre = new javax.swing.JTextField();
        BTNAGREGAR = new javax.swing.JButton();
        BTNELIMINAR = new javax.swing.JButton();
        BTNEDITAR = new javax.swing.JButton();
        btnACTUALIZAR = new javax.swing.JButton();
        BTNSALIRADMI = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        TXTContraseña = new javax.swing.JTextField();
        txtHora = new javax.swing.JLabel();
        txtMinutos = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtSegundos = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        TXTApellido = new javax.swing.JTextField();
        TXTECivil = new javax.swing.JTextField();
        TXTDNI = new javax.swing.JTextField();
        TXTUsuario = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        TXTSexo = new javax.swing.JComboBox<>();
        BTNMODIFICAR = new javax.swing.JButton();
        TXTOcupacion = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TBADMI.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "NOMBRE", "APELLIDOS", "DNI", "USUARIO", "CONTRASEÑA", "SEXO", "ESTADO CIVIL", "OCUPACION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, false, false, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TBADMI.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(TBADMI);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 290, 620, 270));

        TXTNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTNombreActionPerformed(evt);
            }
        });
        getContentPane().add(TXTNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 100, 156, 27));

        BTNAGREGAR.setText("AGREGAR USUARIO");
        BTNAGREGAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNAGREGARActionPerformed(evt);
            }
        });
        getContentPane().add(BTNAGREGAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 290, 139, 40));

        BTNELIMINAR.setText("ELIMINAR USUARIO");
        BTNELIMINAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNELIMINARActionPerformed(evt);
            }
        });
        getContentPane().add(BTNELIMINAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 350, 140, 40));

        BTNEDITAR.setText("EDITAR DATOS");
        BTNEDITAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNEDITARActionPerformed(evt);
            }
        });
        getContentPane().add(BTNEDITAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 410, 140, 40));

        btnACTUALIZAR.setText("ACTUALIZAR");
        btnACTUALIZAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnACTUALIZARActionPerformed(evt);
            }
        });
        getContentPane().add(btnACTUALIZAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 470, 108, 40));

        BTNSALIRADMI.setText("SALIR");
        BTNSALIRADMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNSALIRADMIActionPerformed(evt);
            }
        });
        getContentPane().add(BTNSALIRADMI, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 530, 80, 30));

        jLabel13.setFont(new java.awt.Font("Tw Cen MT", 3, 18)); // NOI18N
        jLabel13.setText("Hora:");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 30, -1, -1));

        TXTContraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTContraseñaActionPerformed(evt);
            }
        });
        getContentPane().add(TXTContraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 160, 156, 27));

        txtHora.setFont(new java.awt.Font("Tw Cen MT", 3, 18)); // NOI18N
        txtHora.setText("00");
        getContentPane().add(txtHora, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 30, -1, -1));

        txtMinutos.setFont(new java.awt.Font("Tw Cen MT", 3, 18)); // NOI18N
        txtMinutos.setText("00");
        getContentPane().add(txtMinutos, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 30, -1, -1));

        jLabel15.setFont(new java.awt.Font("Tw Cen MT", 3, 18)); // NOI18N
        jLabel15.setText(":");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 30, -1, -1));

        jLabel14.setFont(new java.awt.Font("Tw Cen MT", 3, 18)); // NOI18N
        jLabel14.setText(":");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 30, -1, -1));

        txtSegundos.setFont(new java.awt.Font("Tw Cen MT", 3, 18)); // NOI18N
        txtSegundos.setText("00");
        getContentPane().add(txtSegundos, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 30, -1, -1));

        jLabel11.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 3, 36)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("REGISTROS DE USUARIOS");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, -1, -1));

        TXTApellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTApellidoActionPerformed(evt);
            }
        });
        getContentPane().add(TXTApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 100, 156, 27));

        TXTECivil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTECivilActionPerformed(evt);
            }
        });
        getContentPane().add(TXTECivil, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 230, 156, 27));

        TXTDNI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTDNIActionPerformed(evt);
            }
        });
        getContentPane().add(TXTDNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 90, 156, 27));

        TXTUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTUsuarioActionPerformed(evt);
            }
        });
        getContentPane().add(TXTUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 160, 156, 27));

        jLabel2.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nombres");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Usuario");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, -1, -1));

        jLabel4.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Apellidos");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Contraseña");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 150, -1, 50));

        jLabel6.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Estado Civil");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("DNI");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 100, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Sexo");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 150, -1, -1));

        jLabel9.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Ocupación");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 230, -1, -1));

        TXTSexo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar:", "MASCULINO", "FEMENINO" }));
        TXTSexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTSexoActionPerformed(evt);
            }
        });
        getContentPane().add(TXTSexo, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 160, 160, -1));

        BTNMODIFICAR.setText("CONFIRMAR MODIFICACION");
        BTNMODIFICAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNMODIFICARActionPerformed(evt);
            }
        });
        getContentPane().add(BTNMODIFICAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 220, 180, 40));

        TXTOcupacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar:", "Administrador", "Secretaria/o", "Presidente", "" }));
        TXTOcupacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTOcupacionActionPerformed(evt);
            }
        });
        getContentPane().add(TXTOcupacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 230, 160, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/wallpaper_latino_de_intensamente__by_dwowforce-d8xveoj.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-290, 0, 1190, 610));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BTNELIMINARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNELIMINARActionPerformed
       usuario.eliminar(modelo);
    }//GEN-LAST:event_BTNELIMINARActionPerformed

    private void TXTNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TXTNombreActionPerformed

    private void BTNSALIRADMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNSALIRADMIActionPerformed
        Eleccion1 m=new Eleccion1();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();       
    }//GEN-LAST:event_BTNSALIRADMIActionPerformed

    private void BTNAGREGARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNAGREGARActionPerformed
    if((TXTUsuario.getText()).equals(TXTNombre.getText()+"."+ TXTDNI.getText())){
       int escoger =JOptionPane.showConfirmDialog(this,"Esta seguro que desea registrar","Confirmacion de registar",JOptionPane.YES_NO_OPTION);

                switch(escoger){
                    case JOptionPane.YES_OPTION:
                        usuario.registrar(modelo);
                        break;
                    case JOptionPane.NO_OPTION:

                } 
    }else{
       JOptionPane.showMessageDialog(null,"El nombre de usuario debe tener el formato : nombre.DNI ");
    }
        
    }//GEN-LAST:event_BTNAGREGARActionPerformed

    private void TXTContraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTContraseñaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TXTContraseñaActionPerformed
    private void TXTApellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTApellidoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TXTApellidoActionPerformed
    private void TXTECivilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTECivilActionPerformed

    }//GEN-LAST:event_TXTECivilActionPerformed
    private void TXTDNIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTDNIActionPerformed

    }//GEN-LAST:event_TXTDNIActionPerformed
    private void TXTUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTUsuarioActionPerformed
     
    }//GEN-LAST:event_TXTUsuarioActionPerformed
    private void TXTSexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTSexoActionPerformed

    }//GEN-LAST:event_TXTSexoActionPerformed

    private void BTNEDITARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNEDITARActionPerformed
        
        
        int i = TBADMI.getSelectedRow();
        
        TXTNombre.setText(modelo.getValueAt(i, 0).toString());
        TXTApellido.setText(modelo.getValueAt(i, 1).toString());
        TXTDNI.setText(modelo.getValueAt(i, 2).toString());
        TXTUsuario.setText(modelo.getValueAt(i, 3).toString());
        TXTContraseña.setText(modelo.getValueAt(i,4).toString());
        TXTSexo.setSelectedItem(modelo.getValueAt(i,5).toString());
        TXTECivil.setText(modelo.getValueAt(i, 6).toString());
        TXTSexo.setSelectedItem(modelo.getValueAt(i,7).toString());

    }//GEN-LAST:event_BTNEDITARActionPerformed

    private void BTNMODIFICARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNMODIFICARActionPerformed
        
        usuario.editar(modelo);
        //PARA ACTUALIZAR LOS DATOS
        TXTNombre.setText("");
        TXTApellido.setText("");
        TXTDNI.setText("");
        TXTUsuario.setText("");
        TXTContraseña.setText("");
        TXTSexo.setSelectedItem("");
        TXTECivil.setText("");
        TXTSexo.setSelectedItem("");

    }//GEN-LAST:event_BTNMODIFICARActionPerformed

    private void btnACTUALIZARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnACTUALIZARActionPerformed
        GestiondeUsuarios GestiondeUsuarios=new GestiondeUsuarios();
        GestiondeUsuarios.setLocationRelativeTo(null);
        GestiondeUsuarios.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnACTUALIZARActionPerformed

    private void TXTOcupacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTOcupacionActionPerformed

    }//GEN-LAST:event_TXTOcupacionActionPerformed

    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GestiondeUsuarios().setVisible(true);
            }
        });
    }
    
    public class cronometro implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
           
            GregorianCalendar tiempo=new GregorianCalendar();
            int hora,minutos,segundos;
            hora=tiempo.get(Calendar.HOUR);
            minutos=tiempo.get(Calendar.MINUTE);
            segundos=tiempo.get(Calendar.SECOND);
            
            txtHora.setText(String.valueOf(hora));
            txtMinutos.setText(String.valueOf(minutos));
            txtSegundos.setText(String.valueOf(segundos));
            
        }
       
    } 
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTNAGREGAR;
    private javax.swing.JButton BTNEDITAR;
    private javax.swing.JButton BTNELIMINAR;
    private javax.swing.JButton BTNMODIFICAR;
    private javax.swing.JButton BTNSALIRADMI;
    public static javax.swing.JTable TBADMI;
    public static javax.swing.JTextField TXTApellido;
    public static javax.swing.JTextField TXTContraseña;
    public static javax.swing.JTextField TXTDNI;
    public static javax.swing.JTextField TXTECivil;
    public static javax.swing.JTextField TXTNombre;
    public static javax.swing.JComboBox<String> TXTOcupacion;
    public static javax.swing.JComboBox<String> TXTSexo;
    public static javax.swing.JTextField TXTUsuario;
    private javax.swing.JButton btnACTUALIZAR;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JLabel txtHora;
    public static javax.swing.JLabel txtMinutos;
    public static javax.swing.JLabel txtSegundos;
    // End of variables declaration//GEN-END:variables
}
