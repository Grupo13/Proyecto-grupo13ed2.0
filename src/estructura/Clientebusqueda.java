package estructura;

import Metodos.conexion;
import Metodos.procedimientos_BD_BUSQUEDA;
import Metodos.procedimientos_BD_PSICOLOGOS;
import static estructura.Gperfiles.conn;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import oracle.net.aso.p;


public class Clientebusqueda extends javax.swing.JFrame {
    
    procedimientos_BD_BUSQUEDA psicologobusqueda = new procedimientos_BD_BUSQUEDA();
    DefaultTableModel modelo = new DefaultTableModel();
    
    static Connection conn = null;
    //PARA LA CONEXION
    static Statement s = null;
    //PARA ESTABLECER UNA CONSULTA
    static ResultSet rs = null;
    //PARA BRINDAR UN RESULTADO
    
    int i;
   procedimientos_BD_BUSQUEDA p = new procedimientos_BD_BUSQUEDA();
    
    public Clientebusqueda() {
        initComponents();
        TABBUSQCLIENTE.addMouseListener(new MouseAdapter(){
             
            public void mouseClicked(MouseEvent e){
                 i= TABBUSQCLIENTE.getSelectedRow();   
            }
        });
         System.out.println("todo esta been");
         TABBUSQCLIENTE.getTableHeader().setReorderingAllowed(false) ;//evita que se pueda mover las columnas cuando se ejecuta
         TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel( 
            p.mostrar(),new String [] {"Nombre", "Apellido", "SEXO", "ESPECIALIDAD","DISTRITO","AÑOS DE \n EXPERIECIA","Propuesta \nSalarial"}
                                                                         )
                                ); 
 System.out.println("todo esta been");
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs = conexion.BUSQUEDA(rs);
             System.out.println("todo esta been1");
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs.getMetaData();
             System.out.println("todo esta been2");
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
             System.out.println("todo esta been3");
            //Establecer como cabezeras el nombre de las colunnas
            for (int i = 1; i <= cantidadColumnas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));
            }
             System.out.println("todo esta been4");
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        }
        System.out.println("todo esta been");
        psicologobusqueda.mostraryrecolecciondedatos(modelo);
         System.out.println("todo esta been21");
       this.TABBUSQCLIENTE.setModel(modelo);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        jComboBox2 = new javax.swing.JComboBox<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        TABBUSQCLIENTE = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Propuesta salarial", "10-20", "20-30", "30-40", "40-50", "50-60" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(307, 68, 136, -1));

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Distrito", "Ancon", "Ate", "Barranco", "Breña", "Carabayllo", "Chaclacayo", "Chorrillos", "Cieneguilla", "Comas", "El Agustino", "Independencia", "Jesús María", "La Molina", "La Victoria", "Lima", "Lince", "Los Olivos", "Lurigancho", "Lurín", "Magdalena del Mar", "Miraflores", "Pachacamac", "Pucusana", "Pueblo Libre", "Puente Piedra", "Punta Hermosa", "Punta Negra", "Rimac", "San Bartolo", "San Borja", "San Isidro", "San Juan de Lurigancho", "San Juan de Miraflores", "San Luis", "San Martín de Porres", "San Miguel", "Santa Anita", "Santa María del Mar", "Santa Rosa", "Santiago de Surco", "Surquillo", "Villa El Salvador" }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(69, 68, 113, -1));

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "TERAPIA INDIVIDUAL", "TERAPIA DE PAREJAS", "TERAPIA EN ADICCION", "TERAPIA DE FAMILIA" }));
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBox3, new org.netbeans.lib.awtextra.AbsoluteConstraints(565, 68, 125, -1));

        TABBUSQCLIENTE.setAutoCreateRowSorter(true);
        TABBUSQCLIENTE.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "NOMBRE", "APELLIDOS", "SEXO", "ESPECIALIDAD", "DISTRITO", "AÑOS DE EXPERIENCIA ", "PRESUPUESTO SALARILA", "GRADO ACADEMICO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TABBUSQCLIENTE.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        TABBUSQCLIENTE.setFocusable(false);
        TABBUSQCLIENTE.setMaximumSize(new java.awt.Dimension(2147483647, 320));
        TABBUSQCLIENTE.setMinimumSize(new java.awt.Dimension(655, 320));
        TABBUSQCLIENTE.setPreferredSize(new java.awt.Dimension(775, 320));
        TABBUSQCLIENTE.setUpdateSelectionOnSort(false);
        jScrollPane1.setViewportView(TABBUSQCLIENTE);
        TABBUSQCLIENTE.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(57, 153, 660, 320));

        jButton1.setText("VOLVER");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(498, 496, -1, -1));

        jButton2.setText("SELECCIONAR ");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(219, 496, -1, -1));

        jLabel1.setText("Especialidad");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 47, 75, -1));

        jLabel2.setText("Propuesta salarial");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(322, 47, -1, -1));

        jLabel3.setText("Distrito");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(102, 47, -1, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/wallpaper_latino_de_intensamente__by_dwowforce-d8xveoj.jpg"))); // NOI18N
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(-390, -60, 1150, 640));
        jLabel4.getAccessibleContext().setAccessibleName("jLabel4");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
            String variable = String.valueOf(jComboBox1.getSelectedItem());
               TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel( 
            
            p.buscarXpsal(variable, p.mostrar().length)
            ,new String [] {"Nombre", "Apellido", "SEXO", "ESPECIALIDAD","DISTRITO","AÑOS DE \n EXPERIECIA","Propuesta \nSalarial"}


        ));     
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
    String variable = String.valueOf(jComboBox2.getSelectedItem());
        

        TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel( 
            
            p.buscarXdis(variable, p.mostrar().length),new String [] {"Nombre", "Apellido", "SEXO", "ESPECIALIDAD","DISTRITO","AÑOS DE \n EXPERIECIA","Propuesta \nSalarial"}


        ));     }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
 String variable = String.valueOf(jComboBox3.getSelectedItem());
        

        TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel( 
            
            p.buscarXesp(variable, p.mostrar().length)
            ,new String [] {"Nombre", "Apellido", "SEXO", "ESPECIALIDAD","DISTRITO","AÑOS DE \n EXPERIECIA","Propuesta \nSalarial"}


        ));     }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Eleccion1 obj=new Eleccion1();
        obj.setLocationRelativeTo(null);
        obj.setVisible(true);
        dispose();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
         p.seleccionar(i);
        Selecciondeperfil vista = new Selecciondeperfil();
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        dispose();       
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Clientebusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Clientebusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Clientebusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Clientebusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Clientebusqueda().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TABBUSQCLIENTE;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}

