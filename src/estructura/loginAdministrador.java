package estructura;

import Metodos.MetodosLoginAdministrador;
import javax.swing.JOptionPane;

public class loginAdministrador extends javax.swing.JFrame {

    public loginAdministrador() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbUSUARIO = new javax.swing.JLabel();
        INGRESAR = new javax.swing.JButton();
        PasswordADMIN = new javax.swing.JPasswordField();
        lbADMI = new javax.swing.JLabel();
        SALIR = new javax.swing.JButton();
        txtADMIN = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbUSUARIO.setText("USUARIO");
        getContentPane().add(lbUSUARIO, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, 68, -1));

        INGRESAR.setText("Ingresar");
        INGRESAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                INGRESARActionPerformed(evt);
            }
        });
        getContentPane().add(INGRESAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 170, -1, -1));

        PasswordADMIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PasswordADMINActionPerformed(evt);
            }
        });
        getContentPane().add(PasswordADMIN, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 120, 202, -1));

        lbADMI.setText("CONTRASEÑA");
        getContentPane().add(lbADMI, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 130, -1, -1));

        SALIR.setText("Salir");
        SALIR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SALIRActionPerformed(evt);
            }
        });
        getContentPane().add(SALIR, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 170, -1, -1));

        txtADMIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtADMINActionPerformed(evt);
            }
        });
        getContentPane().add(txtADMIN, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 80, 202, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/21362231-Psicolog-a-tarjeta-hecha-a-mano-de-personajes-de-papel-sobre-fondo-azul-Render-3D-Concepto-de-negoci-Foto-de-archivo.jpg"))); // NOI18N
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtADMINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtADMINActionPerformed

    }//GEN-LAST:event_txtADMINActionPerformed

    private void INGRESARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_INGRESARActionPerformed
         MetodosLoginAdministrador metodosloginadmi = new MetodosLoginAdministrador();
        if(metodosloginadmi.validar_ingreso()==1){
                  
                    this.dispose();

                    JOptionPane.showMessageDialog(null, "Bienvenido\n Has ingresado "
                    + "satisfactoriamente al sistema", "Mensaje de bienvenida",
                    JOptionPane.INFORMATION_MESSAGE);

                     GestiondeUsuarios formformularioAdmi = new GestiondeUsuarios();
                     formformularioAdmi.setLocationRelativeTo(null);
                    formformularioAdmi.setVisible(true);
                    dispose();

        }else {
                    
                    JOptionPane.showMessageDialog(null, "Acceso denegado:\n"
                    + "Por favor ingrese un usuario y/o contraseña correctos", "Acceso denegado",
                    JOptionPane.ERROR_MESSAGE);
            
        }
              
    }//GEN-LAST:event_INGRESARActionPerformed

    private void SALIRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SALIRActionPerformed
        Eleccion2 m=new Eleccion2();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();
    }//GEN-LAST:event_SALIRActionPerformed

    private void PasswordADMINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PasswordADMINActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PasswordADMINActionPerformed

    public static void main(String args[]) {
      
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(loginAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new loginAdministrador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton INGRESAR;
    public static javax.swing.JPasswordField PasswordADMIN;
    private javax.swing.JButton SALIR;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel lbADMI;
    private javax.swing.JLabel lbUSUARIO;
    public static javax.swing.JTextField txtADMIN;
    // End of variables declaration//GEN-END:variables
}
