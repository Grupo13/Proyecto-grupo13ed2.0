package Metodos;
//referencias de conexion 
import java.sql.*;

public class conexion {
   
static Connection conn=null;
static Statement st=null;
static ResultSet rs=null;
static ResultSet rs1=null;
static ResultSet rs2=null;

static String bd="DATABASE2123";
static String login="michelle";
static String password="michelle2123";
static String url="jdbc:oracle:thin:@localhost:1521:DATABASE2123";

//EN QUE MAQUINA SE ENCUENTRA LA BASE DATOS 

public static Connection Enlace(Connection conn)throws SQLException {
        try{
            Class.forName("oracle.jdbc.OracleDriver");
            //LE INDICA QUE TIPO DE BASE DE DATOS VA A UTILIZAR
            conn=DriverManager.getConnection(url,login,password);
            //APERTURA LA CONEXION AL MOTOR DE BASE DE DATOS ENVIANDOLE EL URL DONDE SE ENCUENTRA LA BD Y EL USUARIO Y LA CLAVE
            System.out.printf("Base de datos encontrada y conectada");
        }
        catch (ClassNotFoundException e){
            System.out.printf("Base de datos no encontrada");
            //SI NO EXISTIERA LA BD ENVIARIA ESTE MENSAJE
        }
        return conn;
}
public static Statement sta(Statement st)throws SQLException {
    conn=Enlace(conn);
    st=conn.createStatement();
    //PRARA EJECUTAR UNA CONSULTA
    System.out.println("Se ejecuto la consulta");
    return st;
}

public static ResultSet USUARIO(ResultSet rs)throws SQLException {
    st=sta(st);
    rs=st.executeQuery("select * from USUARIO");
    //UTLIZA EL ST PARA EJECUTAR LA SENTENCIA SQL 
    
    return rs;
}
public static ResultSet PSICOLOGO(ResultSet rs1)throws SQLException {
    st=sta(st);
    rs1=st.executeQuery("select * from PSICOLOGOS");
    //UTLIZA EL ST PARA EJECUTAR LA SENTENCIA SQL 
    return rs1;
} 
public static ResultSet BUSQUEDA(ResultSet rs2)throws SQLException {
    st=sta(st);
    rs2=st.executeQuery("select NOMBRES,APELLIDOS,ESPECIALIDAD,SEXO,PROPUESTA_SALARIAL,AÑOS_LABORAL,DISTRITO from PSICOLOGOS");
    //UTLIZA EL ST PARA EJECUTAR LA SENTENCIA SQL 
    
    return rs2;
}

}