package Metodos;

import static Metodos.conexion.conn;
import static Metodos.conexion.rs;
import static Metodos.conexion.rs1;
import static Metodos.conexion.rs2;
import static estructura.Gperfiles.lblfoto;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.StringTokenizer;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class procedimientos_BD_BUSQUEDA {
        ListaPsicologos lista = new ListaPsicologos();
        String nombre, apellido, sexo,
          especialidad, distrito, aexp, psalaria,fnac,CMP, gacademico, Telfono, email,dni;
     
    
    public void mostraryrecolecciondedatos(DefaultTableModel modelo){
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs1 = conexion.PSICOLOGO(rs1);
            rs2=conexion.BUSQUEDA(rs2);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd1 = rs1.getMetaData();
            ResultSetMetaData rsMd2 = rs2.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnasPsicologo = rsMd1.getColumnCount();
            int cantidadColumnasBusqueda = rsMd2.getColumnCount();
            
            //Creando las filas para la tabla de busqueda con los campos especificos
            while (rs2.next()) {
                Object[] fila1 = new Object[cantidadColumnasBusqueda];
                for (int i = 0; i < cantidadColumnasBusqueda; i++) {
                    fila1[i] = rs2.getObject(i + 1);
                }
                modelo.addRow(fila1);
            }
            
            //Creando las filas para la tabla de busqueda con todos los campos 
            while (rs1.next()) {
                Object[] fila2 = new Object[cantidadColumnasPsicologo];
                for (int i = 0; i < cantidadColumnasPsicologo; i++) {
                    fila2[i] = rs1.getObject(i + 1);
                    if(i==0){
                      nombre = fila2[i].toString();
                    }if(i==1){
                      apellido = fila2[i].toString();  
                    }if(i==2){
                      especialidad = fila2[i].toString();  
                    }if(i==3){
                      CMP = fila2[i].toString();  
                    }if(i==4){
                      dni = fila2[i].toString();  
                    }if(i==5){
                      distrito = fila2[i].toString();  
                    }if(i==6){
                      fnac = fila2[i].toString();  
                    }if(i==7){
                      psalaria = fila2[i].toString();  
                    }if(i==8){
                      sexo = fila2[i].toString();  
                    }if(i==9){
                      Telfono = fila2[i].toString();  
                    }if(i==10){
                      email = fila2[i].toString();  
                    }if(i==11){
                      gacademico = fila2[i].toString();  
                    }if(i==13){
                      aexp = fila2[i].toString();  
                    }
          
                }
            }
            rs.close();
            conn.close();
            
            lista.addnodo(nombre, apellido, sexo, especialidad, fnac, CMP, gacademico, Telfono, email, dni, distrito, aexp, psalaria);
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al mostrar \n" + ex);
        }
    }
    
    public String[][] mostrar(){
              
             
        String matriz[][]=new String[lista.getTam()][7];
        for (int i = 0; i <lista.getTam(); i++) {
            
           matriz[i][0]=lista.mostrarNodo("nombre", i);
            matriz[i][1]=lista.mostrarNodo("apellido", i);
            matriz[i][2]=lista.mostrarNodo("sexo", i);
            matriz[i][3]=lista.mostrarNodo("especialidad", i);
            matriz[i][4]=lista.mostrarNodo("distrito", i);
            matriz[i][5]=lista.mostrarNodo("experiencia", i);
            matriz[i][6]=lista.mostrarNodo("salario", i);
   
        }
        return matriz;    
     }
 
     public String[][] buscarXdis(String Distrito,int tam){
     String matriz[][]=new String[tam][7];
     int i = 0;
        for(int j =0; j < tam; j++){
            
            if(lista.mostrarNodo("distrito", j).compareTo(Distrito)==0){
            
            matriz[i][0]=lista.mostrarNodo("nombre", j);
            matriz[i][1]=lista.mostrarNodo("apellido", j);
            matriz[i][2]=lista.mostrarNodo("sexo", j);
            matriz[i][3]=lista.mostrarNodo("especialidad", j);
            matriz[i][4]=lista.mostrarNodo("distrito", j);
            matriz[i][5]=lista.mostrarNodo("experiencia", j);
            matriz[i][6]=lista.mostrarNodo("salario", j);
            i++;

            }
        }
            
        return matriz;
        
    }


     public String[][] buscarXesp(String especialidad,int tam){
     String matriz[][]=new String[tam][7];
     int i = 0;
        for(int j =0; j < tam; j++){
            //.toUpperCase()->convierte el texto en mayuscula xq n el combobox esta en mayuscula
            if(lista.mostrarNodo("especialidad", j).toUpperCase().compareTo(especialidad)==0){
            
            matriz[i][0]=lista.mostrarNodo("nombre", j);
            matriz[i][1]=lista.mostrarNodo("apellido", j);
            matriz[i][2]=lista.mostrarNodo("sexo", j);
            matriz[i][3]=lista.mostrarNodo("especialidad", j);
            matriz[i][4]=lista.mostrarNodo("distrito", j);
            matriz[i][5]=lista.mostrarNodo("experiencia", j);
            matriz[i][6]=lista.mostrarNodo("salario", j);
            i++;
            
            }
        }
            
        return matriz;
        
    }

public String[][] buscarXpsal(String psalarial,int tam){
     String matriz[][]=new String[tam][7];
     int i = 0;
     StringTokenizer mistokens=new StringTokenizer(psalarial, "-");
     int val1= Integer.parseInt(mistokens.nextToken());
      int val2= Integer.parseInt(mistokens.nextToken());
        
      for(int j =0; j < tam; j++){
            //.toUpperCase()->convierte el texto en mayuscula xq n el combobox esta en mayuscula
            if(Integer.parseInt(lista.mostrarNodo("salario", j))>=val1 && Integer.parseInt(lista.mostrarNodo("salario", j)) <=val2){
            
            matriz[i][0]=lista.mostrarNodo("nombre", j);
            matriz[i][1]=lista.mostrarNodo("apellido", j);
            matriz[i][2]=lista.mostrarNodo("sexo", j);
            matriz[i][3]=lista.mostrarNodo("especialidad", j);
            matriz[i][4]=lista.mostrarNodo("distrito", j);
            matriz[i][5]=lista.mostrarNodo("experiencia", j);
            matriz[i][6]=lista.mostrarNodo("salario", j);
            i++;
            
            }
        }
      
        return matriz;
    
}


public void seleccionar(int row){
      //  int seleccion = 
                JOptionPane.showOptionDialog(
	    	                   null,
			           "Nombre: "+lista.mostrarNodo("nombre", row)
                                    +"\nApellidos: "+lista.mostrarNodo("apellido", row) //El mensaje que aparece en el dialog
                                    +"\nEspecialidad: "+lista.mostrarNodo("especialidad", row)
                                    +"\nE - mail: "+lista.mostrarNodo("apellido", row)
                                    +"\nSexo: "+lista.mostrarNodo("sexo", row)
                                    +"\nGrado Academico: "+lista.mostrarNodo("apellido", row)
                                    +"\nDistrito: "+lista.mostrarNodo("distrito", row)
                                    +"\nCMP: "+lista.mostrarNodo("apellido", row)
                                    +"\nDNI: "+lista.mostrarNodo("apellido", row)
                                    +"\nTELEFONO: "+lista.mostrarNodo("apellido", row)
                                    +"\nEdad: "+lista.mostrarNodo("apellido", row)
                                    +"\nPropuesta Salarial: "+lista.mostrarNodo("salario", row)
                                    +"\nAños de experiencia: "+lista.mostrarNodo("experiencia", row)
                        , 
			           "Perfil de Usuario",
			           JOptionPane.YES_NO_CANCEL_OPTION,//Botones que apareces para seleccionar
			           JOptionPane.QUESTION_MESSAGE,//Icono por defecto
			           null,    // null para icono por defecto, o un icono personalizado
			           new Object[] { "Imprimir Informacion", "Salir"},   // botones presonalizados  null 
			   								  //botones por defecto en este caso
			                                                                  // YES, NO y CANCEL
			           "opcion 1");
}

/*private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {                                             
        String sql="select foto, nombre from PSICOLOGOS where codigo = "+txtcodigo.getText();
        ImageIcon foto;
        InputStream is;
        String nombre;
        try{
            ResultSet rs = con.ejecutarSQLSelect(sql);
            while(rs.next()){
                is = rs.getBinaryStream(1);
                nombre = rs.getString(2);
                
                BufferedImage bi = ImageIO.read(is);
                foto = new ImageIcon(bi);
                
                Image img = foto.getImage();
                Image newimg = img.getScaledInstance(140, 170, java.awt.Image.SCALE_SMOOTH);
                
                ImageIcon newicon = new ImageIcon(newimg);
                
                lblfoto.setIcon(newicon);
                txtnombre.setText(nombre);
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(rootPane,"exception: "+ex);
        }
    }
  */   
               
}
